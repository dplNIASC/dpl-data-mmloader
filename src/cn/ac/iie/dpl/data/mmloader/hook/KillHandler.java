package cn.ac.iie.dpl.data.mmloader.hook;

import cn.ac.iie.dpl.data.mmloader.globalParas.GlobalParas;
import org.apache.log4j.Logger;
import sun.misc.Signal;
import sun.misc.SignalHandler;

public class KillHandler implements SignalHandler {

    public static Logger log = Logger.getLogger(KillHandler.class.getName());

    public void registerSignal(String signalName) {
        Signal signal = new Signal(signalName);
        Signal.handle(signal, this);
    }

    @Override
    public void handle(Signal signal) {
        log.info("now receive the system signal " + signal.getName() + " " + signal.getNumber() + "\n\n");
        if (signal.getName().equals("TERM")) {
            GlobalParas.isShouldExit.set(true);
            log.info("after receive the kill TERM signal,will stop the consumer ");
        } else if ((signal.getName().equals("INT")) || (signal.getName().equals("HUP"))) {
            GlobalParas.isShouldExit.set(true);
            log.info("after receive the kill INT or HUP signal,will stop the consumer ");           
        } else {
            log.info("can not process the system signal " + signal.toString());
        }
    }
}
