/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cn.ac.iie.dpl.data.mmloader.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author MyPC
 */
public class ConfigureUtil {
    String cfgFile;
    Properties proCfg;

    public ConfigureUtil() throws FileNotFoundException, IOException {
        cfgFile = "dpl-data-mmloader.properties";
        System.out.println(this.cfgFile);
        FileInputStream inStream = new FileInputStream(this.cfgFile);
        proCfg = new Properties();
        proCfg.load(inStream);
    }

    public ConfigureUtil(String f) throws FileNotFoundException, IOException {
        cfgFile = f;
        FileInputStream inStream = new FileInputStream(this.cfgFile);
        proCfg = new Properties();
        proCfg.load(inStream);
    }

    public String getProperty(String k) {
        return proCfg.getProperty(k);
    }

    public int getIntProperty(String k) {
        return Integer.parseInt(proCfg.getProperty(k));
    }

    public long getLongProperty(String k) {
        return Long.parseLong(proCfg.getProperty(k));
    }

    public Boolean getBooleanProperty(String k) {
        if ("true".equalsIgnoreCase(proCfg.getProperty(k))) {
            return true;
        }
        return false;
    }
}
